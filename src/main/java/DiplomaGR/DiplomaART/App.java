package DiplomaGR.DiplomaART;


import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.derby.tools.sysinfo;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.thymeleaf.standard.expression.Each;

import DiplomaGR.DiplomaART.data.CoverageHandler;
import DiplomaGR.DiplomaART.data.AcoperireRepository;
import DiplomaGR.DiplomaART.data.CoverageLine;
import DiplomaGR.DiplomaART.data.NormRepository;
import DiplomaGR.DiplomaART.data.TimetableLine;
import DiplomaGR.DiplomaART.data.TimetableLineRepository;
import DiplomaGR.DiplomaART.data.PayLine;
import DiplomaGR.DiplomaART.data.Teacher;
import DiplomaGR.DiplomaART.data.TeacherRepository;
import DiplomaGR.DiplomaART.data.TimeTableHandler;




/**
 * Hello world!
 *
 */

@SpringBootApplication
public class App 
{
	
    public static void main( String[] args ) {
    	
        SpringApplication.run(App.class, args);
   }
    
    
    @Bean
    public CommandLineRunner startOfApplication(TeacherRepository teacherRepository,
    											TimetableLineRepository tr,
    											AcoperireRepository ar,
    											NormRepository nR) {
       return (args) -> {
    	   Watcher a = new Watcher(tr,teacherRepository,nR,ar);
    	   a.start();
    	   System.out.println("END CommandLineRunner");
    	   return;
    	   
    	   
    	   //ac.readingExcelAcoperire();
//    	   
//    	   int i = 0;
//    	   for (LinieAcoperire acope : ar.findByAcoperitContaining("Draghici")) {
//    		 //  System.out.println(acope);
//    		   String prs= "p";
//    		   for (LinieOrar s : tr.findByMaterieContaining(acope.getTitluDisciplina().toUpperCase())) {
//					prs = s.getPrescurtare();
//					break;
//    		   } 
//    		   System.out.println(	i++ +" " + acope.getGrad().substring(1)+ acope.getPost()+" " + "A&C " +
//    		   						prs   +" " + "2" +" " +acope.getAn_gr()+" "+ "data" +" " + acope.getOra());
//    	   }
//      	   
//    	   LocalDate start = LocalDate.of(2017, 2, 21);
//    	   LocalDate end = LocalDate.of(2017, 4,29);
//
//    	    DayOfWeek dowOfStart = start.getDayOfWeek();
//    	    int difference = DayOfWeek.MONDAY.getValue() - dowOfStart.getValue();
//    	    if (difference < 0) difference += 7;
//
//    	    List<LocalDate> schoolDays = new ArrayList<LocalDate>();
//
//    	    LocalDate day = start;
//    	    do {
//    	        schoolDays.add(day);
//    	        day = day.plusDays(1);
//    	    } while (day.isBefore(end));
//
//    	    System.out.println("Fridays in range: " + schoolDays);
//    	    for (LocalDate s : schoolDays) {
//				System.out.println(s.format( DateTimeFormatter.ofPattern("dd.MMM")));
//			}
//      	   
//      	   i=1;
//    	    for (LocalDate d : schoolDays) {
//    	    	
//	    	    for (LinieAcoperire acope : ar.findByAcoperitContaining("Draghici")) {
//	    	    	String prs= "p";
//	    	    	for (LinieOrar s : tr.findByMaterieContaining(acope.getTitluDisciplina().toUpperCase())) {
//	 					prs = s.getPrescurtare();
//	 					break;
//	    	    	}
//	    	    	if(d.getDayOfWeek().getValue()==acope.getZi())
//	    	    	{
//	    	    		System.out.println(	i++ +" " + acope.getGrad().substring(1)+ acope.getPost()+" " + "A&C " +
//	    	    							prs   +"    " + "2" +" " +acope.getAn_gr()+" "+ d.format( DateTimeFormatter.ofPattern("dd.MMM")) +" " + acope.getOra());
//
//	    	    		//new LiniePlata(	i++ ,acope.getGrad().substring(1), acope.getPost() , "A&C " ,"    " , "2" ,acope.getAn_gr(),d.format( DateTimeFormatter.ofPattern("dd.MMM")) , acope.getOra());
//	    	    	}
//	    	    }
//    	    }
//    	   
//    	   //System.out.println(t11.toString());
       };
    }
}
