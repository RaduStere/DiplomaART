package DiplomaGR.DiplomaART.data;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;


public class RoomsTimeTableHandler {
    private String path;
    Vector<Room> rooms;

    public RoomsTimeTableHandler(String path) {
        this.path = path;
        this.rooms = new Vector<Room>();
    }


    private void readingExcelRoomsFile_xls()  {
        FileInputStream file = null;
        HSSFWorkbook workbook = null;
        try {
            file = new FileInputStream(new File(path));
            //Create Workbook instance holding reference to .xls file
            workbook = new HSSFWorkbook(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        //Get first/desired sheet from the workbook
        HSSFSheet sheet = workbook.getSheetAt(0);

        //Iterate through each rows one by one
        Iterator<Row> rowIterator = sheet.iterator();
        Iterator<Cell> cellIterator;
        ArrayList<Row> roomsSheet = new ArrayList<>();
        Cell cell;
        int rowCounter = sheet.getPhysicalNumberOfRows();

        while (rowCounter-- != 0) {
            roomsSheet.add(rowIterator.next());
        }

        for(int i = 0; i < sheet.getNumMergedRegions(); ++i) {
            CellRangeAddress range = sheet.getMergedRegion(i);
            int firstRow = range.getFirstRow();
            int firstColumn = range.getFirstColumn();
            int lastRow = range.getLastRow();
            int lastColumn = range.getLastColumn();
            if( 1 >= firstRow ) continue;
            if(!roomsSheet.get(firstRow).getCell(firstColumn).toString().isEmpty()){
                rooms.add(new Room(roomsSheet.get(firstRow).getCell(firstColumn).toString()));
            }
        }
    }

}
