package DiplomaGR.DiplomaART.data;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
public class TimetableLine {
	
	@Transient
	private static int ID=0;
	
	
	@Id
	@Column(name="ID")
	public int id;
	

	@Column(name="cadruDidactic")
    public String teacher;
	
	@Column(name="zi")
	public String day;
	
	@Column(name="zi_nr")
	public int dayNumber;
	
	@Column(name="ORASTART")
	public int startHour;
	
	@Column(name="ORASTOP")
	public int stopHour;
	
	@Column(name="MATERIE")
	public String subject;
	
	@Column(name="PRESCURTAREMATERIE")
	public String subjectAcronim;
	
	@Column(name="tip_ora")
	public String classType;
	
    @Column(name="SALA")
    public String room;
    
    @Column(name="GRUPE")
    public String groups;

    
    @Transient
    public ArrayList<String> groupsArray;
    
    @Column(name="tip_materie_paritate")
    public String subjectEvenness;
    
    @Column(name="paritate")
    public int evenness;
    
    @Column(name="facultate")
    private String college;
    @Column(name="specializare")
    private String specialization;

	@Column(name="an_de_studii")
    private String studyYear;
	@Column(name="SERIE")
    private String series;
	@Column(name="formaDeInvatamant")
    private String fromOfEducation;
	@Column(name="semestru")
    private String semestrer;
	@Column(name="anUniversitar")
    private String academicYear;

    
    public TimetableLine() {
    	subject = "";
    	subjectAcronim = "";
    	teacher="";
    	room="";
    }

    private TimetableLine(String zi, int oraStart, int oraFinal, ArrayList<String> grupe,int paritate,String facultate, String specializare,String anDeStudii,String serie,String formaDeInvatamant,String semestru,String anUniversitar) {
        this();
    	this.id = ID;
        ID++;
    	this.day = zi.toUpperCase();
    	if(zi.equals("LUNI"))
    		dayNumber = 1;
    	if(zi.equals("MARTI"))
    		dayNumber = 2;
    	if(zi.equals("MIERCURI"))
    		dayNumber = 3;
    	if(zi.equals("JOI"))
    		dayNumber = 4;
    	if(zi.equals("VINERI"))
    		dayNumber = 5;
    	
    	
    	
    	
    	
        this.startHour = oraStart;
        this.stopHour = oraFinal;
        this.groupsArray = grupe;
        this.evenness = paritate;
        switch (paritate){
            case 0:
                this.subjectEvenness = "saptamanal";
                break;
            case 1:
                this.subjectEvenness = "impar";
                break;
            case 2:
                this.subjectEvenness = "par";
                break;


        }
        this.college = facultate;
        this.specialization=specializare;
        this.studyYear = anDeStudii;
        this.series = serie;
        this.fromOfEducation = formaDeInvatamant;
        this.semestrer = semestru;
        this.academicYear = anUniversitar;


    }

    public TimetableLine(String day, int startHour, int endHour, String text, ArrayList<String> grupe,int paritate,String facultate, String specializare,String anDeStudii,String serie,String formaDeInvatamant,String semestru,String anUniversitar) {
        this(day, startHour, endHour, grupe,paritate,facultate,specializare,anDeStudii,serie, formaDeInvatamant, semestru,anUniversitar);
        text = stringCleanUp(text);
        if(text.isEmpty()) return;

        if(text.toLowerCase().contains("sport")){
            this.subjectAcronim = "Sport";
            this.subject = "Sport";
            this.classType = "sport";
            this.room = "sala de sport";
            return;
        }


        if(text.contains("(c)")){
            curs(text);
        }
        else {
            nuCurs(text);
        }
        System.out.println(toString());

    }

    public void nuCurs(String text) {

        String[] b = text.split("\\(|\\)|-|,");
        int i = 0;
        for (String a:b) {
            if (a.isEmpty()) continue;

            switch (i) {

                case 0:
                    this.subjectAcronim = a;
                    break;
                case 1:
                    this.classType = a;
                    break;
                case 2:
                    this.room = a;
                    break;
            }
            i++;
        }
        i++;
    }

    public void curs(String text) {



        String[] b = text.split("\\(|\\)|-|,");

        int i= 0;
        for (String a:b) {
            if (a.isEmpty()) continue;

            switch (i) {

                case 0:
                    this.subject = a;
                    break;
                case 1:
                    if(a.equals("c")){
                        this.classType = a;
                        i++;
                        break;
                    }
                    this.subjectAcronim = a;
                    break;
                case 2:
                    this.classType = a;
                    break;
                case 3:
                    this.room = a;
                    break;
                case 4:
                    this.teacher = a;
            }
            i++;
        }
        i++;
    }

    public String stringCleanUp(String text) {
        //text cleanup
        String auxText = text.replace("  ", " ");
        while(!text.equals(auxText)) { text = auxText;auxText = text.replace("  ", " ");}
        //remove not useful spaces
        text = text.replace(", "  ,",");
        text = text.replace(") "  ,")");
        text = text.replace("( "  ,"(");
        text = text.replace(" )"  ,")");
        text = text.replace(" ("  ,"(");
        text = text.replace("- "  ,"-");
        text = text.replace(" - " ,"-");
        text = text.replace(" -"  ,"-");
        return text;
    }

    @Override
    public String toString() {
        String grp ="";
        for (String s : groupsArray) grp += s +", ";
        grp.substring(0, grp.length()-1);
        return    "ID = "+id+ " " +
        		"LinieOrar{" +
                "  zi='"                + day                            + '\'' +
                ", oraStart='"          + startHour                      + '\'' +
                ", oraFinal='"          + stopHour                      + '\'' +
                ", materie='"           + subject                       + '\'' +
                ", materiePrescurtare='"+ subjectAcronim            + '\'' +
                ", cadru didactic='"    + teacher                 + '\'' +
                ", tipOra='"            + classType                        + '\'' +
                ", sala='"              + room                          + '\'' +
                ", grupe='"             + grp                           + '\'' +
                ", paritate='"          + subjectEvenness            + '\'' +
                ", paritate_int='"      + evenness                      + '\'' +
                ",facultate = "         +college                      + '\'' +
                ",specializare = "      +specialization                   + '\'' +
                ",anDeStudii = "        +studyYear                     + '\'' +
                ",serie = "             +series                          + '\'' +
                ",formaDeInvatamant = " +fromOfEducation              + '\'' +
                ",semestru = "          +semestrer                       + '\'' +
                ",anUniversitar = "     +academicYear                  + '\'' +
                '}';
    }
    public static int getID() {
		return ID;
	}

	public static void setID(int iD) {
		ID = iD;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCadruDidactic() {
		return teacher;
	}

	public void setCadruDidactic(String cadruDidactic) {
		this.teacher = cadruDidactic;
	}

	public String getZi() {
		return day;
	}

	public void setZi(String zi) {
		this.day = zi;
	}

	public int getOraStart() {
		return startHour;
	}

	public void setOraStart(int oraStart) {
		this.startHour = oraStart;
	}

	public int getOraFinal() {
		return stopHour;
	}

	public void setOraFinal(int oraFinal) {
		this.stopHour = oraFinal;
	}

	public String getMaterie() {
		return subject;
	}

	public void setMaterie(String materie) {
		this.subject = materie;
	}

	public String getPrescurtare() {
		return subjectAcronim;
	}

	public void setPrescurtare(String prescurtare) {
		this.subjectAcronim = prescurtare;
	}

	public String getTipOra() {
		return classType;
	}

	public void setTipOra(String tipOra) {
		this.classType = tipOra;
	}

	public String getSala() {
		return room;
	}

	public void setSala(String sala) {
		this.room = sala;
	}

	public ArrayList<String> getGrupe() {
		return groupsArray;
	}

	public void setGrupe(ArrayList<String> grupe) {
		this.groupsArray = grupe;
	}

	public String getTipMaterieParitate() {
		return subjectEvenness;
	}

	public void setTipMaterieParitate(String tipMaterieParitate) {
		this.subjectEvenness = tipMaterieParitate;
	}

	public int getParitate() {
		return evenness;
	}

	public void setParitate(int paritate) {
		this.evenness = paritate;
	}

	public String getFacultate() {
		return college;
	}

	public void setFacultate(String facultate) {
		this.college = facultate;
	}

	public String getSpecializare() {
		return specialization;
	}

	public void setSpecializare(String specializare) {
		this.specialization = specializare;
	}

	public String getAnDeStudii() {
		return studyYear;
	}

	public void setAnDeStudii(String anDeStudii) {
		this.studyYear = anDeStudii;
	}

	public String getSerie() {
		return series;
	}

	public void setSerie(String serie) {
		this.series = serie;
	}

	public String getFormaDeInvatamant() {
		return fromOfEducation;
	}

	public void setFormaDeInvatamant(String formaDeInvatamant) {
		this.fromOfEducation = formaDeInvatamant;
	}

	public String getSemestru() {
		return semestrer;
	}

	public void setSemestru(String semestru) {
		this.semestrer = semestru;
	}

	public String getAnUniversitar() {
		return academicYear;
	}

	public void setAnUniversitar(String anUniversitar) {
		this.academicYear = anUniversitar;
	}

}
