package DiplomaGR.DiplomaART.data;

import org.springframework.data.repository.CrudRepository;

public interface NormRepository extends CrudRepository<NormLine, String> {

}
