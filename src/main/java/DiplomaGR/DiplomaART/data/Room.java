package DiplomaGR.DiplomaART.data;

import javax.persistence.Entity;
import javax.persistence.Id;

import groovy.transform.ToString;

@Entity
public class Room {
	
	@Id
	
	private String room;

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public Room(String room) {
		super();
		this.room = room;
	}
	public String toString() {
		return room.toString();
	}
}
