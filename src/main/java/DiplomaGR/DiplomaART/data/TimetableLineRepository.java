package DiplomaGR.DiplomaART.data;

import org.springframework.data.repository.CrudRepository;


public interface TimetableLineRepository extends CrudRepository<TimetableLine, String>{

	Iterable<TimetableLine> findAllByOrderBySubjectAcronimAsc();
	
	Iterable<TimetableLine> findBysubjectAcronimContaining(String inputValue);

	Iterable<TimetableLine> findBysubjectContaining(String inputValue);

	Iterable<TimetableLine> findByRoomContaining(String inputValue);
	
	Iterable<TimetableLine> findByStartHour(String inputValue);


	Iterable<TimetableLine> findByDayContaining(String inputValue);

	Iterable<TimetableLine> findBySubject(String titluDisciplina);

	
	
	//Iterable<LinieOrar> findAllCadruDidacticISNOTNULL();

}
