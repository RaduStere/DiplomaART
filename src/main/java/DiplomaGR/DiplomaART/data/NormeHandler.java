package DiplomaGR.DiplomaART.data;


import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

public class NormeHandler {
    private ArrayList<NormLine> data;
    private String path;
    @Resource
    AcoperireRepository ar;
    @Resource
    TimetableLineRepository linieOrarRepository;
    @Resource
    TeacherRepository teacherRepository;
    @Resource
    NormRepository normRepository;
    

    @Override
    public String toString() {
        String tmp ="";
        for (NormLine s : data) tmp += s +"\n";

        return "NormeHandler{" +
                "data='\n" + tmp +
                "', path='" + path + '\'' +
                '}';
    }


    public NormeHandler(String path,NormRepository nr) {
        this.path = path;
        data = new ArrayList<>();
        normRepository = nr;
    }

    public void readFile() {
        try {
            FileInputStream file = new FileInputStream(new File(path));
            Iterator<Row> rowIterator;
            if(path.charAt(path.length()-1) == 'x' ) rowIterator = getRowIterator_xlsx(file);
            else rowIterator = getRowIterator_xls(file);


            rowIterator.next();//skip the table header


            Iterator<Cell> cellIterator;
            String post;
            while (rowIterator.hasNext()) {
                cellIterator = rowIterator.next().cellIterator();
                post =  cellIterator.next().toString();
                if(post.isEmpty()) {
                    break;                //presupun ca daca nu am nimic la post sa terminat fisierul
                }
                NormLine l = new NormLine(
                        post,                           //  post;
                        cellIterator.next().toString(), //  tip;
                        cellIterator.next().toString(), //  nume;
                        cellIterator.next().toString(), //  tip1;
                        cellIterator.next().toString(), //  noLabel1;
                        cellIterator.next().toString(), //  titluDisciplina;
                        cellIterator.next().toString(), //  faculte;
                        cellIterator.next().toString(), //  forma;
                        cellIterator.next().toString(), //  cod;
                        cellIterator.next().toString(), //  an;
                        cellIterator.next().toString(), //  serie;
                        cellIterator.next().toString(), //  Nr;
                        cellIterator.next().toString(), //  noLaber2;
                        cellIterator.next().toString(), //  C1;
                        cellIterator.next().toString(), //  A1;
                        cellIterator.next().toString(), //  Mc1;
                        cellIterator.next().toString(), //  M1;
                        cellIterator.next().toString(), //  C2;
                        cellIterator.next().toString(), //  A2;
                        cellIterator.next().toString(), //  Mc2;
                        cellIterator.next().toString(), //  M2;
                        cellIterator.next().toString(), //  MT;
                        cellIterator.next().toString(), //  oreAn;
                        cellIterator.next().toString()  //  grup;
                        );
                data.add(l);
                normRepository.save(l);
            }



            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        
        
    }
    
   

    
    
    private Iterator<Row> getRowIterator_xlsx(FileInputStream file) throws IOException {
        //Create Workbook instance holding reference to .xls file
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        //Get first/desired sheet from the workbook
        XSSFSheet sheet = workbook.getSheetAt(0);

        //Iterate through each rows one by one
        return sheet.iterator();
    }
    private Iterator<Row> getRowIterator_xls(FileInputStream file) throws IOException {
        //Create Workbook instance holding reference to .xls file
        HSSFWorkbook workbook = new HSSFWorkbook(file);

        //Get first/desired sheet from the workbook
        HSSFSheet sheet = workbook.getSheetAt(0);

        //Iterate through each rows one by one
        return sheet.iterator();
    }
}
