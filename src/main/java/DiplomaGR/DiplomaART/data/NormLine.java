package DiplomaGR.DiplomaART.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class NormLine {
	
	@Transient
	private static int ID=0;
	
	@Id
	private int 	id;
    private int     post;
    private String  tip;
    private String  nume;
    private String  tip2;
    private String  tip_ora;
    private String  titluDisciplina;
    private String  faculte;
	private String  forma;
    private String  cod;
    private int     an;
    private String  serie;
    private int     Nr;
    private String  tip_organizare; //grupa/semigrupa
    private int     C1;
    private int     A1;
    private double  Mc1;
    private double  M1;
    private int     C2;
    private int     A2;
    private double  Mc2;
    private double  M2;
    private double  MT;
    private double  oreAn;
    private String  grup;

    public NormLine() {
    	
    }
    public NormLine(String post, String tip, String nume, String tip2, String tip_ora, String titluDisciplina, String faculte, String forma, String cod, String an, String serie, String nr, String noLaber2, String c1, String a1, String mc1, String m1, String c2, String a2, String mc2, String m2, String MT, String oreAn, String grup) {
    	this();
    	this.id = ID;
    	ID++;
        this.nume               = nume;
        this.tip                = tip;
        this.tip2               = tip2;
        this.tip_ora = tip_ora;
        this.titluDisciplina    = titluDisciplina;
        this.faculte            = faculte;
        this.forma              = forma;
        this.cod                = cod;
        this.serie              = serie;
        this.tip_organizare = noLaber2;
        this.grup               = grup;

        try {
            if (!post.isEmpty())    this.post   = Double.valueOf(post).intValue();
            if (!an.isEmpty())      this.an     = Double.valueOf(an).intValue();
            if (!nr.isEmpty())      this.Nr     = Double.valueOf(nr).intValue();
            if (!c1.isEmpty())      this.C1     = Double.valueOf(c1).intValue();
            if (!a1.isEmpty())      this.A1     = Double.valueOf(a1).intValue();
            if (!mc1.isEmpty())     this.Mc1    = Double.valueOf(mc1);
            if (!m1.isEmpty())      this.M1     = Double.valueOf(m1);
            if (!c2.isEmpty())      this.C2     = Double.valueOf(c2).intValue();
            if (!a2.isEmpty())      this.A2     = Double.valueOf(a2).intValue();
            if (!mc2.isEmpty())     this.Mc2    = Double.valueOf(mc2);
            if (!m2.isEmpty())      this.M2     = Double.valueOf(m2);
            if (!MT.isEmpty())      this.MT     = Double.valueOf(MT);
            if (!oreAn.isEmpty())   this.oreAn  = Double.valueOf(oreAn);
        } catch (NumberFormatException e) {
            this.post =this.an =this.Nr =this.C1 =this.A1 =this.C2 =this.A2 = -1;
            this.Mc1 =this.M1 =this.Mc2 =this.M2 =this.MT =this.oreAn= -1.0;

            e.printStackTrace();
        }
    }


    @Override
    public String toString() {
        return "LinieNorma{" +
                "post=" + post                              +
                ", tip='" + tip                             + '\'' +
                ", nume='" + nume                           + '\'' +
                ", tip2='" + tip2                           + '\'' +
                ", tip_ora='" + tip_ora + '\'' +
                ", titluDisciplina='" + titluDisciplina     + '\'' +
                ", faculte='" + faculte                     + '\'' +
                ", forma='" + forma                         + '\'' +
                ", cod='" + cod                             + '\'' +
                ", an=" + an                                +
                ", serie='" + serie                         + '\'' +
                ", Nr=" + Nr                                +
                ", tip_organizare='" + tip_organizare + '\''+
                ", C1=" + C1                                +
                ", A1=" + A1                                +
                ", Mc1=" + Mc1                              +
                ", M1=" + M1                                +
                ", C2=" + C2                                +
                ", A2=" + A2                                +
                ", Mc2=" + Mc2                              +
                ", M2=" + M2                                +
                ", MT=" + MT                                +
                ", oreAn=" + oreAn                          +
                ", grup='" + grup                           + '\'' +
                '}';
    }
    
    public int getPost() {
		return post;
	}


	public void setPost(int post) {
		this.post = post;
	}


	public String getTip() {
		return tip;
	}


	public void setTip(String tip) {
		this.tip = tip;
	}


	public String getNume() {
		return nume;
	}


	public void setNume(String nume) {
		this.nume = nume;
	}


	public String getTip2() {
		return tip2;
	}


	public void setTip2(String tip2) {
		this.tip2 = tip2;
	}


	public String getTip_ora() {
		return tip_ora;
	}


	public void setTip_ora(String tip_ora) {
		this.tip_ora = tip_ora;
	}


	public String getTitluDisciplina() {
		return titluDisciplina;
	}


	public void setTitluDisciplina(String titluDisciplina) {
		this.titluDisciplina = titluDisciplina;
	}


	public String getFaculte() {
		return faculte;
	}


	public void setFaculte(String faculte) {
		this.faculte = faculte;
	}


	public String getForma() {
		return forma;
	}


	public void setForma(String forma) {
		this.forma = forma;
	}


	public String getCod() {
		return cod;
	}


	public void setCod(String cod) {
		this.cod = cod;
	}


	public int getAn() {
		return an;
	}


	public void setAn(int an) {
		this.an = an;
	}


	public String getSerie() {
		return serie;
	}


	public void setSerie(String serie) {
		this.serie = serie;
	}


	public int getNr() {
		return Nr;
	}


	public void setNr(int nr) {
		Nr = nr;
	}


	public String getTip_organizare() {
		return tip_organizare;
	}


	public void setTip_organizare(String tip_organizare) {
		this.tip_organizare = tip_organizare;
	}


	public int getC1() {
		return C1;
	}


	public void setC1(int c1) {
		C1 = c1;
	}


	public int getA1() {
		return A1;
	}


	public void setA1(int a1) {
		A1 = a1;
	}


	public double getMc1() {
		return Mc1;
	}


	public void setMc1(double mc1) {
		Mc1 = mc1;
	}


	public double getM1() {
		return M1;
	}


	public void setM1(double m1) {
		M1 = m1;
	}


	public int getC2() {
		return C2;
	}


	public void setC2(int c2) {
		C2 = c2;
	}


	public int getA2() {
		return A2;
	}


	public void setA2(int a2) {
		A2 = a2;
	}


	public double getMc2() {
		return Mc2;
	}


	public void setMc2(double mc2) {
		Mc2 = mc2;
	}


	public double getM2() {
		return M2;
	}


	public void setM2(double m2) {
		M2 = m2;
	}


	public double getMT() {
		return MT;
	}


	public void setMT(double mT) {
		MT = mT;
	}


	public double getOreAn() {
		return oreAn;
	}


	public void setOreAn(double oreAn) {
		this.oreAn = oreAn;
	}


	public String getGrup() {
		return grup;
	}


	public void setGrup(String grup) {
		this.grup = grup;
	}


    
}
