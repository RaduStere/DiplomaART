package DiplomaGR.DiplomaART.data;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.annotation.Resource;

/**
 * Created by Radu Stere on 25.02.2017.
 *
 */
public class TimeTableHandler {

    private String titlu;
    private String path;
    private String college;
    private String specialization;
    private String yearOfStudy;
    private String serie;
    private ArrayList<String> grupe;
    private String formaDeInvatamant;
    private String semestru;
    private String scholarYear;
    private ArrayList<TimetableLine> processedTimetable;
    

    private TimetableLineRepository tr;
	
	private TeacherRepository teacherRepository;
    
    @Override
    public String toString() {
        String grp ="";
        for (String s : grupe) grp += s +"\n";
        grp = grp.substring(0, grp.length()-1);
        return "TimeTableHandler" +
                "{" + " ,\n" +
                "titlu='"             + titlu             + '\'' + " ,\n" +
                "path='"              + path              + '\'' + " ,\n" +
                "facultate='"         + college         + '\'' + " ,\n" +
                "specializare='"      + specialization      + '\'' + " ,\n" +
                "anDeStudii='"        + yearOfStudy        + '\'' + " ,\n" +
                "serie='"             + serie             + '\'' + " ,\n" +
                "formaDeInvatamant='" + formaDeInvatamant + '\'' + " ,\n" +
                "semestru='"          + semestru          + '\'' + " ,\n" +
                "anUniversitar='"     + scholarYear     + '\'' + " ,\n" +
                "grupe='"             + grp               + '\'' + " ,\n" +
                '}';
    }

    public TimeTableHandler(String path,TimetableLineRepository tr2, TeacherRepository tr3) {
        grupe = new ArrayList<>();
        this.path = path;
        college = specialization = yearOfStudy = serie = formaDeInvatamant = scholarYear = "";
        processedTimetable = new ArrayList<>();
        this.tr= tr2;
        this.teacherRepository=tr3;
        
    }

    public static void main(String [] args) {


 //       TimeTableHandler t11 = new TimeTableHandler("input_files_easy_mode\\orar\\Orar1CA.xls");t11.readingExcelTimeTable(); System.out.println(t11);
//        TimeTableHandler t12 = new TimeTableHandler("D:\\sources\\DriftingBlade\\input_files_easy_mode\\orar\\Orar3CB.xls");t12.readingExcelTimeTable(); System.out.println(t12);
//        TimeTableHandler t13 = new TimeTableHandler("D:\\sources\\DriftingBlade\\input_files_easy_mode\\orar\\Orar3CC.xls");t13.readingExcelTimeTable(); System.out.println(t13);

//        TimeTableHandler t1  = new TimeTableHandler("D:\\sources\\DriftingBlade\\input_files_easy_mode\\orar\\Orar3CA.xls"); t1.readingExcelTimeTable(); System.out.println(t1 );
//        TimeTableHandler t2  = new TimeTableHandler("D:\\sources\\DriftingBlade\\input_files\\Orar1CB.xls");t2.readingExcelTimeTable(); System.out.println(t2);
//        TimeTableHandler t3  = new TimeTableHandler("D:\\sources\\DriftingBlade\\input_files\\Orar1CC.xls");t3.readingExcelTimeTable(); System.out.println(t3);
//        TimeTableHandler t4  = new TimeTableHandler("D:\\sources\\DriftingBlade\\input_files\\Orar1CD.xls");t4.readingExcelTimeTable(); System.out.println(t4);
//        TimeTableHandler t5  = new TimeTableHandler("D:\\sources\\DriftingBlade\\input_files\\Orar4C2.xls");t5.readingExcelTimeTable(); System.out.println(t5);
//        TimeTableHandler t6  = new TimeTableHandler("D:\\sources\\DriftingBlade\\input_files\\Orar4C3.xls");t6.readingExcelTimeTable(); System.out.println(t6);
//        TimeTableHandler t7  = new TimeTableHandler("D:\\sources\\DriftingBlade\\input_files\\Orar4C4.xls");t7.readingExcelTimeTable(); System.out.println(t7);
  //      String programSali = "input_files_easy_mode\\organizare_sali\\Program de folosire sali curs si seminar IS_CTI_2016-2017_sem2.xls";
        	//System.out.println(programSali);


    }



    public void readingExcelTimeTable() {
        //System.out.println(orarPrelucrat.toString());
        try {
            if(path.charAt(path.length()-1) == 'x' ) System.out.println("Did not implement ofr xlsx");
            else readingExcelTimeTable_xls();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class MergedOutput{
        private MergedOutput(int time, int originRow, int originColumn, int numberOfSubGrups) {
            this.time = time;
            this.originRow = originRow;
            this.originColumn = originColumn;
            this.numberOfSubGrups = numberOfSubGrups;
        }

        int time;
        int originRow;
        int originColumn;
        int numberOfSubGrups;
    }

    private void readingExcelTimeTable_xls() throws IOException {
        FileInputStream file = new FileInputStream(new File(path));

        //Create Workbook instance holding reference to .xls file
        HSSFWorkbook workbook = new HSSFWorkbook(file);

        //Get first/desired sheet from the workbook
        HSSFSheet sheet = workbook.getSheetAt(0);

        //Iterate through each rows one by one
        Iterator<Row> rowIterator = sheet.iterator();

        readHeaderTimetable(rowIterator);

        readingTimetableData(rowIterator,sheet);
        file.close();
    }

    private void readingTimetableData(Iterator<Row> rowIterator, HSSFSheet sheet) {

        Iterator<Cell> cellIterator;
        ArrayList<Row> orar = new ArrayList<>();
        Cell cell;
        int days = 5;
        int hIntervals = 12;
        int rowCounter = days * hIntervals;
        int column;
        int maxColumn = grupe.size();
        int headerOffset = 10;
        MergedOutput merged;
        String day="";

        while (rowCounter-- != 0) {
            orar.add(rowIterator.next());
        }


        for( int row = 0; row < orar.size()-1; row++) {
        	
            cellIterator = orar.get(row).cellIterator();
            
            column = 0;
            while (cellIterator.hasNext()) {
                cell = cellIterator.next();
                String s = cell.toString();

                if (column == 0 && !s.isEmpty()){
                    day = s;
                }
                column++;
                if(column <= 2) {
                    //System.out.println(s);
                    continue;
                }
                if(cell.toString().isEmpty()){
                    continue;
                }

                Iterator<Cell> tmp = orar.get(row+1).cellIterator();
                Cell aux = cell;

                merged = mergedCell(row+headerOffset,column,sheet);
                 /*
                Din observatii presupun ca un laborator/seminar pe semigrupa va fi scris pe doua celule, una sub alta.
                 */
                // parcurg urmatorul rand pana cand sunt sub celula actuala pentru a lua sala
                for (int i =0;i<column && tmp.hasNext(); i++) {
                    aux = tmp.next();
                    //System.out.println("'"+ i+ " " + aux.toString()+"'");
                }
                String text ="";
                if (!(s.isEmpty() && aux.toString().isEmpty())){ //daca am ceva sub mine le unesc
                 //   System.out.println("'"+ s+ " " + aux.toString()+"'");


                	text = s + aux.toString();
                	aux.setCellValue("");
                	
                }
               // System.out.println(grupe);
              //  System.out.println("(this.grupe.subList("+ merged.originColumn+" - 2 - 1,"+merged.originColumn+" - 2 -1 +"+ merged.numberOfSubGrups);
                ArrayList<String> grupe = new ArrayList<>(this.grupe.subList(merged.originColumn - 2 - 1, merged.originColumn - 2 -1 + merged.numberOfSubGrups));
                int startHour = row % 12 + 8; //ora de input, fiecare zi are 12 intervale de 2 ore si inceoe de la ora 8
                int endHour = startHour + merged.time;

                //work done in here
                parityDivider(day, text, grupe, startHour, endHour);

                aux.setCellValue("");
            }
            //System.out.println("");
        }
    }

    private void parityDivider(String day, String text, ArrayList<String> grupe, int startHour, int endHour) {
        String[] b = text.split(" \\| ");
        int paritate = 0;
        if (text.contains(" | ")) paritate = 1;
        for (String t : b) {
            if(t.isEmpty()){
                paritate++;
                continue;
            }

            asemble_data_and_register(day, grupe, startHour, endHour, paritate, t);
            paritate++;
        }
    }

    @Autowired
	private void asemble_data_and_register(String day, ArrayList<String> grupe, int startHour, int endHour, int paritate, String t) {
        String[] b1 = t.split(" \\|\\| ");
        for (String t1 : b1) {
            if(t1.trim().isEmpty()){
                continue;
            }


            TimetableLine l;
            if (day.contains("Ț")) day = day.replaceAll("Ț", "T");

            l = new TimetableLine(day, startHour, endHour, t1, grupe, paritate,college,specialization,yearOfStudy,serie, formaDeInvatamant, semestru,scholarYear);
            l.setCadruDidactic(l.getCadruDidactic().toUpperCase());
            if(!l.getSala().equals("")) l.setSala(l.getSala().replaceAll(" ", ""));
            
            if(l.getPrescurtare().equals("")) l.setPrescurtare(l.getMaterie());
            if(l.getMaterie().equals("")) 
            	l.setMaterie(l.getPrescurtare());
            
            if (l.getCadruDidactic().contains("Ț")) l.setCadruDidactic(l.getCadruDidactic().replaceAll("Ț", "T"));
            if (l.getCadruDidactic().contains("Ș")) l.setCadruDidactic(l.getCadruDidactic().replaceAll("Ș", "S"));
            if (l.getCadruDidactic().contains("Ă")) l.setCadruDidactic(l.getCadruDidactic().replaceAll("Ă", "A"));
            if (l.getCadruDidactic().contains("î")) l.setCadruDidactic(l.getCadruDidactic().replaceAll("î", "I"));
            
           
            
            l.groups = l.groupsArray.toString();
            
            tr.save(l);
            
            if(!l.getCadruDidactic().equals("")) {
            	teacherRepository.save(new Teacher(l.getCadruDidactic()));
            }
          //  System.out.println(l);
            processedTimetable.add(l);
        }
    }

    private MergedOutput mergedCell(int row, int column, HSSFSheet sheet) {
        int firstRow,firstColumn,lastRow,lastColumn;

        for(int i = 0; i < sheet.getNumMergedRegions(); ++i)
        {
            CellRangeAddress range = sheet.getMergedRegion(i);
            firstRow = range.getFirstRow();
            firstColumn = range.getFirstColumn();
            lastRow = range.getLastRow();
            lastColumn = range.getLastColumn();

            if (    firstRow    <= row      && lastRow      >= row      &&
                    firstColumn <= column   && lastColumn   >= column)
                return new MergedOutput(lastRow-firstRow+1,firstRow-10,firstColumn+1,lastColumn-firstColumn+1); //10 este offsetul de la antet

        }



        return new MergedOutput(2,row-10,column,1); //10 este offsetul de la antet
    }

    private void readHeaderTimetable(Iterator<Row> rowIterator) {
        Row row = rowIterator.next();
        Iterator<Cell> cellIterator = row.cellIterator();
        Cell cell = cellIterator.next();
        titlu = cell.toString();

        rowIterator.next();
        row = rowIterator.next();
        cellIterator = row.cellIterator();
        cell = cellIterator.next();
        college = cell.toString().split(":")[1].substring(1);

        row = rowIterator.next();
        cellIterator = row.cellIterator();
        cell = cellIterator.next();
        specialization = cell.toString().split(":")[1].substring(1);

        row = rowIterator.next();
        cellIterator = row.cellIterator();
        cell = cellIterator.next();
        String tmp = cell.toString();
        String[] aux= tmp.split(" ");
        int i = 0;
        for(String s : aux ) {
            if (!s.isEmpty()){
                ++i;
                //System.out.println(s);
            }
            else continue;

            if(4 == i) {
                yearOfStudy = s;
            }
            if(6 == i || 5 == i) {
                if(s.toLowerCase().equals("serie")) continue;
                serie = s;
            }
        }

        row = rowIterator.next();
        cellIterator = row.cellIterator();
        cell = cellIterator.next();
        tmp = cell.toString();
        aux = tmp.split(" ");
        i = 0;
        for(String s : aux ) {
            if (!s.isEmpty()){
                ++i;
            }
            else continue;


            if(4 == i ) {
                formaDeInvatamant = s + " ";
            }
            if(5 == i) {
                formaDeInvatamant += s;
            }
            if(7 == i) {
                semestru = s;
            }
            if(10 == i ) {
                scholarYear = s + " ";
            }
            if( 12 == i) {
                scholarYear += s;
            }
        }


        rowIterator.next();

        row = rowIterator.next();
        //For each row, iterate through all the columns
        cellIterator = row.cellIterator();

        cellIterator.next();
        cellIterator.next();

        while (cellIterator.hasNext())
        {
            cell = cellIterator.next();

            if (cell.toString().isEmpty()) continue;

            grupe.add(cell.toString()+" A");
            grupe.add(cell.toString()+" B");

            if(cellIterator.hasNext()){
                cellIterator.next();
            }
        }
        rowIterator.next();
        rowIterator.next();

    }



}
