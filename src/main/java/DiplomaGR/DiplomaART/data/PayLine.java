package DiplomaGR.DiplomaART.data;

import java.time.format.DateTimeFormatter;

public class PayLine {
	public int i;
	public String type;
	public String college;
	public String discipline;
	public String course;
	public String aplication;
	public String yearAndGroup;
	public String date;
	public String hour;
	



	public PayLine(int j, String fel, String post, String facultate, String disciplina, String curs,String aplicatii, String an_gr, String data, String ora) {
		this.i = j;
		this.type = fel+post;
		this.college = facultate;
		this.discipline = disciplina;
		this.course = curs;
		this.yearAndGroup = an_gr;
		this.aplication = aplicatii;
		this.date = data;
		this.hour = ora;
	}




	public int getI() {
		return i;
	}




	public void setI(int i) {
		this.i = i;
	}




	public String getFel() {
		return type;
	}




	public void setFel(String fel) {
		this.type = fel;
	}




	public String getFacultate() {
		return college;
	}




	public void setFacultate(String facultate) {
		this.college = facultate;
	}




	public String getDisciplina() {
		return discipline;
	}




	public void setDisciplina(String disciplina) {
		this.discipline = disciplina;
	}




	public String getCurs() {
		return course;
	}




	public void setCurs(String curs) {
		this.course = curs;
	}




	public String getAplicatii() {
		return aplication;
	}




	public void setAplicatii(String aplicatii) {
		this.aplication = aplicatii;
	}




	public String getAn_gr() {
		return yearAndGroup;
	}




	public void setAn_gr(String an_gr) {
		this.yearAndGroup = an_gr;
	}




	public String getData() {
		return date;
	}




	public void setData(String data) {
		this.date = data;
	}




	public String getOra() {
		return hour;
	}




	public void setOra(String ora) {
		this.hour = ora;
	}

	


}
