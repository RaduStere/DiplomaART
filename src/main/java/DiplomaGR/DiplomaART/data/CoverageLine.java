package DiplomaGR.DiplomaART.data;

import javax.persistence.Entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Transient;



@Entity
public class CoverageLine {
	

	@Transient
	private static int ID=0;
	
	@Id
	public String Id;
	
	public String ana;
	public String tipDisciplina;
	public String titluDisciplina;
	public String forma;
	public String cod;
	public String an;
	public String serie;
	public String nr;
	public String grupa_semigrupa;
	public String C2;
	public String A2;
	public String post;
	public String grad;
	public String pers;
	public String tip;
	public String acoperit;
	public String acoperitEfectiv;
	public String an_gr;
	public int zi;
	public String ora;
	public String sala;
	
	


	public CoverageLine(String id, String ana, String tipDisciplina, String titluDisciplina, String forma, String cod,
			String an, String serie, String nr, String grupa_semigrupa, String c2, String a2, String post, String grad,
			String pers, String tip, String acoperit, String acoperitEfectiv, String an_gr, int zi, String ora,
			String sala) {
		this();
		Id = id;
		this.ana = ana;
		this.tipDisciplina = tipDisciplina;
		this.titluDisciplina = titluDisciplina;
		this.forma = forma;
		this.cod = cod;
		this.an = an;
		this.serie = serie;
		this.nr = nr;
		this.grupa_semigrupa = grupa_semigrupa;
		C2 = c2;
		A2 = a2;
		this.post = post;
		this.grad = grad;
		this.pers = pers;
		this.tip = tip;
		this.acoperit = acoperit;
		this.acoperitEfectiv = acoperitEfectiv;
		this.an_gr = an_gr;
		this.zi = zi;
		this.ora = ora;
		this.sala = sala;
	}





	@Override
	public String toString() {
		return "LinieAcoperire [Id=" + Id + ", ana=" + ana + ", tipDisciplina=" + tipDisciplina + ", titluDisciplina="
				+ titluDisciplina + ", forma=" + forma + ", cod=" + cod + ", an=" + an + ", serie=" + serie + ", nr="
				+ nr + ", grupa_semigrupa=" + grupa_semigrupa + ", C2=" + C2 + ", A2=" + A2 + ", post=" + post
				+ ", grad=" + grad + ", pers=" + pers + ", tip=" + tip + ", acoperit=" + acoperit + ", acoperitEfectiv="
				+ acoperitEfectiv + ", an_gr=" + an_gr + ", zi=" + zi + ", ora=" + ora + ", sala=" + sala + "]";
	}





	public CoverageLine() {
		Id = Integer.toString(ID++);
		this.ana = "";
		this.tipDisciplina = "";
		this.titluDisciplina = "";
		this.forma = "";
		this.cod = "";
		this.an = "";
		this.serie = "";
		this.nr = "";
		this.grupa_semigrupa = "";
		C2 = "";
		A2= "";
		this.post = "";
		this.grad = "";
		this.pers = "";
		this.tip = "";
		this.acoperit = "";
		this.acoperitEfectiv = "";
		this.an_gr = "";
		this.zi = 0;
		this.ora = "";
		this.sala = "";
	}
	
	
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getA2() {
		return A2;
	}
	public void setA2(String id) {
		this.A2 = id;
	}
	public String getAna() {
		return ana;
	}
	public void setAna(String ana) {
		this.ana = ana;
	}
	public String getTipDisciplina() {
		return tipDisciplina;
	}
	public void setTipDisciplina(String tipDisciplina) {
		this.tipDisciplina = tipDisciplina;
	}
	public String getTitluDisciplina() {
		return titluDisciplina;
	}
	public void setTitluDisciplina(String titluDisciplina) {
		this.titluDisciplina = titluDisciplina;
	}
	public String getForma() {
		return forma;
	}
	public void setForma(String forma) {
		this.forma = forma;
	}
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	public String getAn() {
		return an;
	}
	public void setAn(String an) {
		this.an = an;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	public String getNr() {
		return nr;
	}
	public void setNr(String nr) {
		this.nr = nr;
	}
	public String getGrupa_semigrupa() {
		return grupa_semigrupa;
	}
	public void setGrupa_semigrupa(String grupa_semigrupa) {
		this.grupa_semigrupa = grupa_semigrupa;
	}
	public String getC2() {
		return C2;
	}
	public void setC2(String c2) {
		C2 = c2;
	}
	public String getPost() {
		return post;
	}
	public void setPost(String post) {
		this.post = post;
	}
	public String getGrad() {
		return grad;
	}
	public void setGrad(String grad) {
		this.grad = grad;
	}
	public String getPers() {
		return pers;
	}
	public void setPers(String pers) {
		this.pers = pers;
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
	public String getAcoperit() {
		return acoperit;
	}
	public void setAcoperit(String acoperit) {
		this.acoperit = acoperit;
	}
	public String getAcoperitEfectiv() {
		return acoperitEfectiv;
	}
	public void setAcoperitEfectiv(String acoperitEfectiv) {
		this.acoperitEfectiv = acoperitEfectiv;
	}
	public String getAn_gr() {
		return an_gr;
	}
	public void setAn_gr(String an_gr) {
		this.an_gr = an_gr;
	}
	public int getZi() {
		return zi;
	}
	public void setZi(String zi) {
		int i = 0;
		if(zi.toUpperCase().equals("LU"))  {			i=10;		}
		if(zi.toUpperCase().equals("MA"))  {			i=20;		}
		if(zi.toUpperCase().equals("MI"))  {			i=30;		}
		if(zi.toUpperCase().equals("JO"))  {			i=40;		}
		if(zi.toUpperCase().equals("VI"))  {			i=50;		}
		if(zi.toUpperCase().equals("SA"))  {			i=60;		}
		if(zi.toUpperCase().equals("DU"))  {			i=70;		}

        if(zi.toUpperCase().equals("LUI")) {			i=11;		}
		if(zi.toUpperCase().equals("MAI")) {			i=21;		}
		if(zi.toUpperCase().equals("MII")) {			i=31;		}
		if(zi.toUpperCase().equals("JOI")) {			i=41;		}
		if(zi.toUpperCase().equals("VII")) {			i=51;		}
		if(zi.toUpperCase().equals("SAI")) {			i=61;		}
		if(zi.toUpperCase().equals("DUI")) {			i=71;		}

        if(zi.toUpperCase().equals("LUP")) {			i=12;		}
		if(zi.toUpperCase().equals("MAP")) {			i=22;		}
		if(zi.toUpperCase().equals("MIP")) {			i=32;		}
		if(zi.toUpperCase().equals("JOP")) {			i=42;		}
		if(zi.toUpperCase().equals("VIP")) {			i=52;		}
		if(zi.toUpperCase().equals("SAP")) {			i=62;		}
		if(zi.toUpperCase().equals("DUP")) {			i=72;		}
        
		
		
		this.zi = i;
	}
	
	public String getOra() {
		return ora;
	}
	public void setOra(String ora) {
		this.ora = ora;
	}
	public String getSala() {
		return sala;
	}
	public void setSala(String sala) {
		this.sala = sala;
	}
	
}
