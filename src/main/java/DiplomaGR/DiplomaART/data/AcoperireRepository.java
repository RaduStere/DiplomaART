package DiplomaGR.DiplomaART.data;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface AcoperireRepository extends CrudRepository<CoverageLine, String> {

	
	Iterable<CoverageLine> findByAcoperit (String inputValue);
	Iterable<CoverageLine> findByAcoperitContaining (String inputValue);

	 
	 @Query(value = "SELECT DISTINCT a.Acoperit FROM coverage_line a", nativeQuery = true)
	Iterable<String> findDistinctAcoperit();
	
}
