package DiplomaGR.DiplomaART.data;


import org.apache.derby.tools.sysinfo;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;

import DiplomaGR.DiplomaART.App;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.annotation.Resource;



public class CoverageHandler {

	

    @Resource
    AcoperireRepository ar;
    @Resource
    TimetableLineRepository linieOrarRepository;
    @Resource
    TeacherRepository teacherRepository;
    @Resource
    NormRepository normRepository;
    
	
	
    private ArrayList<CoverageLine> orarPrelucrat;
	String path;

	
	public CoverageHandler(String path, AcoperireRepository ar) {
		this.path = path;
		this.ar = ar;
	}

	public void readingExcelAcoperire() {
		try {
			FileInputStream file;
			file = new FileInputStream(new File(path));

	        //Create Workbook instance holding reference to .xls file
	        HSSFWorkbook workbook = new HSSFWorkbook(file);

	        //Get first/desired sheet from the workbook
	        HSSFSheet sheet = workbook.getSheetAt(0);

	        //Iterate through each rows one by one
	        Iterator<Row> rowIterator = sheet.iterator();

	        readHeaderAcoperire(rowIterator);

	        readingAcoperireData(rowIterator,sheet);
	        file.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void readingAcoperireData(Iterator<Row> rowIterator, HSSFSheet sheet) {
		System.out.println("Started reading Acoperirea");
		Iterator<Cell> cellIterator;
        ArrayList<Row> acoperire = new ArrayList<>();
        Cell cell;
        
       
        
        
        
        while ( rowIterator.hasNext()  ) {
            acoperire.add(rowIterator.next());
        }
        
        int flag = 1;
        for( int rowNr = 0; rowNr < acoperire.size()-1; rowNr++) {
        	
        	flag = 1;
        	cellIterator = acoperire.get(rowNr).cellIterator();
        	Row row = acoperire.get(rowNr);
        	
        	CoverageLine to_add = new CoverageLine();
        	for(int cn = 0 ; cn < row.getLastCellNum();cn++) {
        		cell = row.getCell(cn,Row.CREATE_NULL_AS_BLANK);    
        		if(!cell.toString().isEmpty()) {
        			
        			linieAcoperireSetter(cell, to_add, cn);
        		}
        		else if(0 == cn) {
        			flag = 0;
        			
        			break;
        		}
        		
        	}
        	if(1 == flag && 0 < row.getLastCellNum() ) {
        		System.out.println(to_add);
        		if(ar != null) {
        			ar.save(to_add);
        		}
        	}
        	
        }
        
		
	}


    public void createHourlyPay(TimetableLineRepository linieOrarRepository,TeacherRepository teacherRepository ) {
    	

        this.linieOrarRepository = linieOrarRepository;
        this.teacherRepository =  teacherRepository;
	
         readingExcelAcoperire();

         LocalDate start = LocalDate.of(2017, 2, 21);
         LocalDate end = LocalDate.of(2017, 4,29);

          DayOfWeek dowOfStart = start.getDayOfWeek();
          int difference = DayOfWeek.MONDAY.getValue() - dowOfStart.getValue();
          if (difference < 0) difference += 7;

          List<LocalDate> schoolDays = new ArrayList<LocalDate>();

          LocalDate day = start;
          do {
              schoolDays.add(day);
              day = day.plusDays(1);
          } while (day.isBefore(end));



     
    	   for (String teacher : ar.findDistinctAcoperit()) {
  				System.out.println(teacher);
  		       ArrayList<PayLine> plat = new ArrayList<>();
        	   int i = 0;
  		       for (CoverageLine acope : ar.findByAcoperitContaining(teacher)) {
  		    	   if (acope.tip.equals("B")) continue;
  		    	   int nr_day = 0;
  		    	   int week_num = 1;
  		    	   
  		    	   for (LocalDate d : schoolDays) {
  		    		   nr_day++;
  		    		   week_num = nr_day/7 + 1;
  		    		   
  		    		   if(d.getDayOfWeek().getValue() == (acope.getZi()/10) && (
  		    				   ((acope.getZi()%10) == 0 ) || (((acope.getZi()%10)%2) == (week_num%2 ) ) 
	    				   ) ) {
  		    			 String duration = Integer.toString(((Integer.parseInt(acope.getOra().substring(3, 5)) - Integer.parseInt(acope.getOra().substring(0, 2)))));
  		    			 
  		    			 plat.add(new PayLine(i++ ,acope.getGrad().substring(1), acope.getPost() , "A&C " ,acope.getTitluDisciplina(),"    " , duration ,acope.getAn_gr(),d.format( DateTimeFormatter.ofPattern("dd.MMM")) , acope.getOra()));
  		    			   
  		    		   }
  		    	   }
  		       }
  				
  				writeFile(plat, teacher);
           }
           
       
  
  
       return;
     
     }

	private void writeFile(ArrayList<PayLine> plat, String teacher) {
		String[] columns = {    "Nr.crt.",
		        "Felul si nr. post",
		        "Facultatea",
		        "Disciplina",
		        "Curs",
		        "Aplicatii",
		        "An/Gr",
		        "Data",
		        "Orele"                 };

		
		// Create a Workbook
		Workbook workbook = new HSSFWorkbook();
		
		CreationHelper createHelper = workbook.getCreationHelper();
		
		// Create a Sheet
		Sheet sheet = workbook.createSheet("Luna");
		
		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 10);
		headerFont.setColor(IndexedColors.BLACK.getIndex());
		
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		
		// Create a Row
		Row headerRow = sheet.createRow(0);
		
		// Create cells
		for(int j = 0; j < columns.length; j++) {
			Cell cell = headerRow.createCell(j);
			cell.setCellValue(columns[j]);
			cell.setCellStyle(headerCellStyle);
		}
		
		// Create Cell Style for formatting Date
		CellStyle dateCellStyle = workbook.createCellStyle();
		dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd.mmm"));
		
		
		// Create Other rows and cells with employees data
		int rowNum = 1;
		for(PayLine pl: plat) {
		    Row row = sheet.createRow(rowNum++);
		    row.createCell(0).setCellValue(pl.getI()           );
		    row.createCell(1).setCellValue(pl.getFel()         );
		    row.createCell(2).setCellValue(pl.getFacultate()   );
		    row.createCell(3).setCellValue(pl.getDisciplina()  );
		    row.createCell(4).setCellValue(pl.getCurs()        );
		    row.createCell(5).setCellValue(pl.getAplicatii()   );
		    row.createCell(6).setCellValue(pl.getAn_gr()       );
		    row.createCell(7).setCellValue(pl.getData()        );
		    row.createCell(8).setCellValue(pl.getOra()         );

		}
		// Write the output to a file
		FileOutputStream fileOut;
		String path = "outputfiles/" + teacher.replace(".","").replace(" ", "").replace(" ", "")+".xls";
		try {
			fileOut = new FileOutputStream(path);
			workbook.write(fileOut);
			fileOut.close();
			this.teacherRepository.save(new Teacher(teacher, teacher, path) );
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Closing the workbook
		//workbook.close();
	}

	
	private void linieAcoperireSetter(Cell cell, CoverageLine to_add, int cn) {
		switch (cn) {
		case 0:
			to_add.setAna(cell.toString());
			break;

		case 1:
			to_add.setTipDisciplina(cell.toString());
			break;

		case 2:
			to_add.setTitluDisciplina(cell.toString());
			break;

		case 3:
			to_add.setForma(cell.toString());
			break;

		case 4:
			to_add.setCod(cell.toString());
			break;

		case 5:
			to_add.setAn(cell.toString());
			break;

		case 6:
			to_add.setSerie(cell.toString());
			break;
		case 7:
			to_add.setNr(cell.toString());
			break;
		case 8:
			to_add.setGrupa_semigrupa(cell.toString());
			break;

		case 9:
			to_add.setC2(cell.toString());
			break;
		case 10:
			to_add.setA2(cell.toString());
			break;
		case 11:
			to_add.setPost(cell.toString());
			break;
		case 12:
			to_add.setGrad(cell.toString());
			break;

		case 13:
			to_add.setPers(cell.toString());
			break;

		case 14:
			to_add.setTip(cell.toString());
			break;

		case 15:
			to_add.setAcoperit(cell.toString());
			break;

		case 16:
			to_add.setAcoperitEfectiv(cell.toString());
			break;

		case 17:
			to_add.setAn_gr(cell.toString());
			break;

		case 18:
			to_add.setZi(cell.toString());
			break;

		case 19:
			to_add.setOra(cell.toString());
			break;

		case 20:
			to_add.setSala(cell.toString());
			break;



		

		default:
			break;
		}
		
		
		
		//System.out.println("CELL: " + cn + " --> " + cell.toString());
	}

	private void readHeaderAcoperire(Iterator<Row> rowIterator) {
		Row row;
		row = rowIterator.next();//first row
		row = rowIterator.next();//second row
		row = rowIterator.next();//third row
		
	}

}
