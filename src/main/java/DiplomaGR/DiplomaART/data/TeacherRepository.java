package DiplomaGR.DiplomaART.data;

import org.springframework.data.repository.CrudRepository;


public interface TeacherRepository extends CrudRepository<Teacher, String> {
	
	Teacher findByName(String name);
	Iterable<Teacher> findByDescription(String name);

}
