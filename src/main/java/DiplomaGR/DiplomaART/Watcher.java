package DiplomaGR.DiplomaART;

import java.io.File;

import DiplomaGR.DiplomaART.data.TimetableLineRepository;
import DiplomaGR.DiplomaART.data.CoverageHandler;
import DiplomaGR.DiplomaART.data.AcoperireRepository;
import DiplomaGR.DiplomaART.data.NormRepository;
import DiplomaGR.DiplomaART.data.NormeHandler;
import DiplomaGR.DiplomaART.data.TeacherRepository;
import DiplomaGR.DiplomaART.data.TimeTableHandler;

public class Watcher extends Thread {
	private TimetableLineRepository tr;
	private TeacherRepository teacherRepository;
	private NormRepository nR;
	private AcoperireRepository ar;
	
    public Watcher(TimetableLineRepository tr, TeacherRepository teacherRepository,NormRepository nR, AcoperireRepository ar) {
		this.tr = tr;
		this.teacherRepository = teacherRepository;
		this.nR = nR;
		this.ar = ar;
	}

	public void run(){
    	
    	while(true) {
    		System.out.println("MyThread running");
	        try {
	        	checkFolderForFiles("dataFromUser\\orar\\");
	        	checkFolderForNorms("dataFromUser\\norme\\");
	        	checkFolderForCoverage("dataFromUser\\acoperire\\");
				Thread.sleep(1000);
				break;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
    	}
    	System.out.println("MyThread quiting");
     }
	

	private void checkFolderForFiles(String path) {
		File folder = new File(path);
	    for (final File fileEntry : folder.listFiles()) {
	        if (!fileEntry.isDirectory()) {
	            System.out.println("Now processing: [" + fileEntry.getAbsolutePath() + "]") ;
	     	   TimeTableHandler t11;
	    	   t11 = new TimeTableHandler(fileEntry.getAbsolutePath(),tr,teacherRepository);	    	   t11.readingExcelTimeTable();
	        }

	    }
	}
	
	private void checkFolderForNorms(String path) {
		File folder = new File(path);
	    for (final File fileEntry : folder.listFiles()) {
	        if (!fileEntry.isDirectory()) {
	            System.out.println("Now processing: [" + fileEntry.getAbsolutePath() + "]") ;
	     	   NormeHandler t11;
	    	   t11 = new NormeHandler(fileEntry.getAbsolutePath(),nR);	    	   t11.readFile();
	    	   
	        }

	    }
	}
	private void checkFolderForCoverage(String path) {
		File folder = new File(path);
	    for (final File fileEntry : folder.listFiles()) {
	        if (!fileEntry.isDirectory()) {
	            System.out.println("Now processing: [" + fileEntry.getAbsolutePath() + "]") ;
	     	   CoverageHandler t11;
	    	   t11 = new CoverageHandler(fileEntry.getAbsolutePath(),ar);	    	   
	    	   t11.createHourlyPay(tr,teacherRepository);
	        }

	    }
	}
}
