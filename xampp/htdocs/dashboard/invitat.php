<?php
include 'header.php';
?>
    <script xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
        document.getElementById("elev").style.display = "none";
        document.getElementById("profesor").style.display = "none";
        document.getElementById("admin").style.display = "none";

    </script>
<?php

$TestereConec = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$pprof;
$mmat;
$ssala;
$zziua;
$sql = "SELECT DISTINCT cadru_didactic FROM timetable_line ORDER BY cadru_didactic";
$result = $TestereConec->query($sql);
$to_encode = array();
$iflagpprofesor = 1;

if ($result->num_rows > 0) {
    //output data of each row
    while ($row = $result->fetch_assoc()) {
        $pprof [$iflagpprofesor] = $row["cadru_didactic"];
        $iflagpprofesor++;
    }
}

$sql = "SELECT DISTINCT MATERIE FROM timetable_line WHERE cadru_didactic <>'' ORDER BY MATERIE ";
$result = $TestereConec->query($sql);
$to_encode = array();
$iflagmmat = 1;

if ($result->num_rows > 0) {
    //output data of each row
    while ($row = $result->fetch_assoc()) {
        $mmat [$iflagmmat] = $row["MATERIE"];
        $iflagmmat++;
    }
}

$sql = "SELECT DISTINCT SALA FROM timetable_line ORDER BY SALA";
$result = $TestereConec->query($sql);
$to_encode = array();
$iflagssala = 1;

if ($result->num_rows > 0) {
    //output data of each row
    while ($row = $result->fetch_assoc()) {
        $ssala [$iflagssala] = $row["SALA"];
        $iflagssala++;
    }
}

$sql = "SELECT DISTINCT zi FROM timetable_line ORDER BY zi_nr";
$result = $TestereConec->query($sql);
$to_encode = array();
$iflagzziua = 1;

if ($result->num_rows > 0) {
    //output data of each row
    while ($row = $result->fetch_assoc()) {
        $zziua [$iflagzziua] = $row["zi"];
        $iflagzziua++;
    }
}

?>

    <div class="box_mijloc">
        <div class="box_tot">
            <div class="left_box">
                <form action="table_invitat.php" method="post">
                    <p>Cauta profesor</p>
                    <select name="c_profesor">
                        <option value="0">Implicit</option>
                        <?php
                        for ($i = 1; $i < $iflagpprofesor; $i++) {
                            echo ' <option value="' . $pprof[$i] . '">' . $pprof[$i] . '</option>';
                        }
                        ?>

                    </select>

                    <p>Cauta Materia</p>
                    <select name="c_materia">
                        <option value="0">Implicit</option>
                        <?php
                        for ($i = 1; $i < $iflagmmat; $i++) {
                            echo ' <option value="' . $mmat[$i] . '">' . $mmat[$i] . '</option>';
                        }
                        ?>
                    </select>

                    <p>Cauta Sala</p>
                    <select name="c_sala">
                        <option value="0">Implicit</option>
                        <?php
                        for ($i = 1; $i < $iflagssala; $i++) {
                            echo ' <option value="' . $ssala[$i] . '">' . $ssala[$i] . '</option>';
                        }
                        ?>
                    </select>
                    <p>Cauta Grupa</p>
                       <input type="text" name="c_grupa" id="c_grupa">
                    <p>Cauta Ziua</p>
                        <select name="c_data">
                            <option value="0">Implicit</option>
                            <?php
                            for ($i = 1; $i < $iflagzziua; $i++) {
                                echo ' <option value="' . $zziua[$i] . '">' . $zziua[$i] . '</option>';
                            }
                            ?>
                    </select>

                    </br>

                    <input style="width: 80px;margin: 25px 0px" type="submit" value="Cautare">
                </form>
            </div>
            <div class="right_box">


                <?php

                ?>
            </div>
        </div>
    </div>

<?php

$TestereConec->close();
include 'footer.php';
?>