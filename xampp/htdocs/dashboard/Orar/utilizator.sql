-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 20 Aug 2018 la 22:17
-- Versiune server: 10.0.34-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mcmbluez_diploma`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `utilizator`
--

CREATE TABLE `utilizator` (
  `Id` int(11) NOT NULL,
  `Utilizator` text CHARACTER SET utf8 NOT NULL,
  `Parola` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Mail` text CHARACTER SET utf8 NOT NULL,
  `Telefon` int(25) NOT NULL,
  `Tip_utilizator` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `utilizator`
--

INSERT INTO `utilizator` (`Id`, `Utilizator`, `Parola`, `Mail`, `Telefon`, `Tip_utilizator`) VALUES
(1, 'Admintest', '80066025484bc243547254a07cfd401d', 'mcm.bluezone@gmail.com', 726447705, 'Admin'),
(2, 'Profesortest', '1a9f99b92e3173e164085b87d7b288cb', 'mcm.bluezone@gmail.com', 726447705, 'Profesori');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `utilizator`
--
ALTER TABLE `utilizator`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `utilizator`
--
ALTER TABLE `utilizator`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
