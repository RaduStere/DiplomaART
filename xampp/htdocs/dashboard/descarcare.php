<?php
include 'header.php';

$connectionToDatabase = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
if ($connectionToDatabase->connect_error) {
    
    echo "ABORT \n" ;
    die("Connection failed: " . $connectionToDatabase->connect_error);
}

$logedInUsername = $_SESSION['user'];

echo "Utilizator: " ;
echo $logedInUsername;
echo "<br>";
echo "Documentul nu este inca generat." ;
echo "<br>";
$sql = "SELECT * FROM teacher";
$result = $connectionToDatabase->query($sql);
echo "<br>"; 
if ($result->num_rows > 0) {
        //output data of each row
        while ($row = $result->fetch_assoc()) {

            if ($logedInUsername == $row["NAME"] && $logedInUsername != $row["DESCRIPTION"] )
            {
                echo $row["NAME"]." ";
                $file_location =  $row["DESCRIPTION"];
                echo $file_location ;
                header('Content-Description: File Transfer');
                header('Content-Type: application/force-download');
                header("Content-Disposition: attachment; filename=\"" . basename($file_location) . "\";");
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file_location));
                ob_clean();
                flush();
                readfile($file_location); 
                  
            }
    }
}


?>


<?php
include 'footer.php';
?>